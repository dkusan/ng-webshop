import { Product } from './product';
import { ShoppingCartProduct } from './shopping-cart-product';


export class ShoppingCart {
  items: ShoppingCartProduct[] = [];

  constructor(private itemsMap: { [productId: number]: ShoppingCartProduct }) 
  {
    this.itemsMap = itemsMap || {};
    for (const productId in itemsMap) 
    {
      const item = itemsMap[productId] as any;
      this.items.push(new ShoppingCartProduct(item.payload.doc.data().product, item.payload.doc.data().quantity,
      item.payload.doc.data().product.firebaseId));
    }
  }

  getQuantity(product: Product) 
  {
      let quantity = 0;
      this.items.forEach(element => {
          if (element.uid === product.uid) {
          quantity = element.quantity;
        }
      });
      return quantity;
  }

  get totalPrice() 
  {
    let sum = 0;
    for (const productId in this.items) {
      sum += this.items[productId].totalPrice;
    }
    return sum;
  }

  get Count() 
  {
    let count = 0;
    for (const item in this.items) {
      count += this.items[item].quantity;
    }
    return count;
  }
}

