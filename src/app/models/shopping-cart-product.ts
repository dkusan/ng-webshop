import { Product } from './product';

export class ShoppingCartProduct {
  
    constructor(public product: Product, public quantity: number, public uid: string) {}

    get totalPrice() { return this.product.price * this.quantity; 
    }
}
