import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Product } from './models/product';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  products: Observable<Product[]>;

  constructor(private db: AngularFirestore) { }

  create(product: Product) {
    return this.db.collection('products').add(product);

  }


  getAll() {
    this.products = this.db.collection('products').snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Product;
        const firebaseId = a.payload.doc.id;
        return { firebaseId, ...data };
      }))
    );
    return this.products;  }

  get(productId) {
    return this.db.collection('products').doc(productId).valueChanges();
  }

  update(productId, product) {
    return this.db.collection('products').doc(productId).update(product);
  }

  delete(productId) {
    return this.db.collection('products').doc(productId).delete();
  }
}
