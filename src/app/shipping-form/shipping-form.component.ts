import { Component, OnInit, Input } from '@angular/core';
import { ShoppingCart } from '../models/shopping-cart';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { OrderService } from '../order.service';
import { Order } from '../models/order';

@Component({
  selector: 'shipping-form',
  templateUrl: './shipping-form.component.html',
  styleUrls: ['./shipping-form.component.css']
})
export class ShippingFormComponent implements OnInit {

  @Input('cart') cart: ShoppingCart;

  shipping = {};
  userId: string;
  subscription: Subscription;

  constructor(private router: Router,
    private authService: AuthService,
    private orderService: OrderService) { }

  ngOnInit() {
    this.subscription = this.authService.user$.subscribe(user => this.userId = user.uid);
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  placeOrder() {
    const order = new Order(this.userId, this.shipping, this.cart);
    this.orderService.storeOrder(order).then((result) => {
      const id = result.id;
      this.router.navigate(['/order-success', id]);
    });
  }
}
