import { Component, OnInit, Input } from '@angular/core';
import { ShoppingCartService } from '../shopping-cart.service';

@Component({
  selector: 'product-quantity',
  templateUrl: './product-quantity.component.html',
  styleUrls: ['./product-quantity.component.css']
})
export class ProductQuantityComponent implements OnInit 
{
  @Input('product') product;
  @Input('shopping-cart') shoppingCart;

  constructor(private shoppingCartService: ShoppingCartService) { }


  ngOnInit() {
  }

  addToCart() 
  {
    this.shoppingCartService.addToCart(this.product);
  }
  removeItem() 
  {
    this.shoppingCartService.removeItem(this.product);
  }

  getQuantity() 
  {
    if (!this.shoppingCart) 
    {
      return 0;
    } else 
    {
      let quantity = 0;
      this.shoppingCart.items.forEach(element => {
        if (element.product.uid === this.product.uid) {
          quantity = element.quantity;
        }
      });
      return quantity;
    }
  }

}
