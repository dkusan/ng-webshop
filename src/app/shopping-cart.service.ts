import {take, map} from 'rxjs/operators';
import {AngularFirestore} from '@angular/fire/firestore';
import {Injectable} from '@angular/core';
import {Product} from './models/product';
import { Observable } from 'rxjs';
import { ShoppingCart } from './models/shopping-cart'


@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  
  constructor(private db: AngularFirestore) {
  }

  private create() {
    return this.db.collection('shoppingCarts').add({
      dateCreated: new Date().getTime()
    });
  }

  async getCart(): Promise<Observable<ShoppingCart>> {
    const cartId = await this.getOrCreateCartId();
    const cart = this.db.collection('shoppingCarts').doc(cartId).collection('items').snapshotChanges()
      .pipe(map(x => new ShoppingCart(x as any)
      ));
    return cart;
  }
  private getItem(cartId: string, productId: string): Observable<any>  {
    return this.db.collection('shoppingCarts').doc(cartId).collection('items').doc(productId).valueChanges();
  }

  private updateItem(cartId: string, productId: string) {
    return this.db.collection('shoppingCarts').doc(cartId).collection('items').doc(productId);
  }

  async removeItem(product: Product) {
    this.updateItemQuantity(product, -1);
  }
  

  private async getOrCreateCartId(): Promise<string> {
    const cartId = localStorage.getItem('cartId');

    if (!cartId) {
      const result = await this.create();
      localStorage.setItem('cartId', result.id);
      return result.id;

    } return cartId;
  }


  // private getOrCreateCartItem(cartId, product) {
  //   this.db.doc('/shopping-carts/' + cartId + '/items/' + product.uid).get()
  //     .toPromise().then((snapshot) => {
  //     if (!snapshot.exists) {
  //       product.quantity = 1;

  //       this.db.doc('/shopping-carts/' + cartId + '/items/' + product.uid).set(product, {merge: true})
  //         .then(() => console.log('Add item to card successful'))
  //         .catch((reason: any) => console.log('Add item to cart failed.', reason));
  //     }
  //   });

  //   return this.db.doc('/shopping-carts/' + cartId + '/items/' + product.uid);
  // }

  async addToCart(product: Product) {
    this.updateItemQuantity(product, 1);
  }

  async clearCart(){
    console.log('cleared cart');
    const cartId = await this.getOrCreateCartId();
    this.db.collection('shoppingCarts').doc(cartId).collection('items').snapshotChanges().pipe(take(1))
    .subscribe(products => {  products.map(productItem =>
      this.db.collection('shoppingCarts').doc(cartId).collection('items').doc(productItem.payload.doc.id).delete()
      );
    });
  }

  private async updateItemQuantity(product: Product, change: number) {
    const cartId = await this.getOrCreateCartId();
    const item$ = this.getItem(cartId, product.uid);

    item$.pipe(take(1)).subscribe(item => {
      if (item  != null) {
        const quantity = item.quantity + change;
        // const quantity = change;
        if (quantity === 0) {this.updateItem(cartId, product.uid).delete(); } else {
          this.updateItem(cartId, product.uid).update({ quantity: quantity }); }
    } else {
      this.updateItem(cartId, product.uid).set({ product: product, quantity: 1 });
      }
    });

  }
  // async addToCart(product: Product) {
  //   const cartId = await this.getOrCreateCartId();
  //   const item$ = this.getOrCreateCartItem(cartId, product);

  //   item$.snapshotChanges().pipe(take(1)).subscribe(item => {
  //     const itemPayload: CartProduct = <CartProduct>item.payload.data();
  //     console.log(itemPayload);
  //     item$.update({...product, quantity: (itemPayload ? itemPayload.quantity : 0) + 1});
  //   });
  // }
}
