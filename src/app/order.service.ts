import { Order } from './models/order';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { ShoppingCartService } from './shopping-cart.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private db: AngularFirestore,
    private shoppingCartService: ShoppingCartService) { }
  
  async storeOrder(order: Order) {
        const orderObj = {
      userId: order.uid,
      datePlaced: order.datePlaced,
      shipping: order.shipping,
      items: order.items
    };
    console.log(orderObj);
    const result = await this.db.collection('/orders').add(orderObj);
    this.shoppingCartService.clearCart();
    return result;
  }
  
  getOrders() {
    return this.db.collection('/orders').snapshotChanges();
  }
  
  getOrdersByUser(userId: string) {
    return this.db.collection('/orders', ref => ref.where('userId', '==', userId)).snapshotChanges();
  }
}
