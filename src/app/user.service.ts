import { AppUser } from './models/ap-user';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private usersCollection: AngularFirestoreCollection<AppUser>;

  constructor(private db: AngularFirestore) {
    this.usersCollection = db.collection<AppUser>('users');
  }

  get(id: string): Observable<AppUser> {
    return this.usersCollection.doc(id).valueChanges() as Observable<AppUser>;
  }

  save(user: firebase.User) {
    this.usersCollection.doc(user.uid).ref.get().then((documentSnapshot) => {
      if (documentSnapshot.exists) {
        this.usersCollection.doc(user.uid).update({
                name: user.displayName,
                email: user.email
              });
      } else {
        this.usersCollection.doc(user.uid).set({
          name: user.displayName,
          email: user.email,
          isAdmin: false
        });
      }
    });
  }
}
